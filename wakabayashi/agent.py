
import numpy as np
import matplotlib
matplotlib.use('Agg') 
import matplotlib.pyplot as plt

import seaborn as sns
import random
import sys
import csv
from policy import Greedy, EpsGreedy, EpsDecGreedy, Q_Learning, Sarsa, Sarsa_lambda, Watkin_lambda, RS_off, RS_on, RS_lam_off, RS_lam_on, RS_tau_on, RS_tau_off

class Agent():
    def __init__(self, value_func="Q_Learning", policy="greedy", learning_rate=0.1, discount_rate=0.9, eps=None, eps_min=None, eps_decrease=None, n_state=None, n_action=None, lamda = 0.9, r_g = 7.5, zeta = 0.05):

        if value_func == "Q_Learning":
            self.value_func = Q_Learning(num_state=n_state, num_action=n_action)
        
        elif value_func == "Sarsa":
            self.value_func = Sarsa(num_state=n_state, num_action=n_action)

        elif value_func == "watkin":
            self.value_func = Watkin_lambda(num_state=n_state, num_action=n_action, lamda = 0.9)

        elif value_func == "Sarsa_lambda":
            self.value_func = Sarsa_lambda(num_state=n_state, num_action=n_action, lamda = 0.9)

        elif value_func == "RS":
            self.value_func = RS_off(num_state=n_state, num_action=n_action, r_g = r_g, zeta = zeta)
        
        elif value_func == "RS_onpolicy":
            self.value_func = RS_on(num_state=n_state, num_action=n_action, r_g = r_g, zeta = zeta)            

        elif value_func == "RS_on_eligibility":
            self.value_func = RS_lam_on(num_state=n_state, num_action=n_action, r_g = r_g, zeta = zeta, learning_rate = learning_rate, discount_rate = discount_rate)

        elif value_func == "RS_off_eligibility":
            self.value_func = RS_lam_off(num_state=n_state, num_action=n_action, r_g = r_g, zeta = zeta, learning_rate = learning_rate, discount_rate = discount_rate)

        elif value_func == "RS_tau_on":
            self.value_func = RS_tau_on(num_state=n_state, num_action=n_action, r_g = r_g, zeta = zeta, learning_rate = learning_rate, discount_rate = discount_rate)

        elif value_func == "RS_tau_off":
            self.value_func = RS_tau_off(num_state=n_state, num_action=n_action, r_g = r_g, zeta = zeta, learning_rate = learning_rate, discount_rate = discount_rate)

        else:
            print("error:価値関数候補が見つかりませんでした")
            sys.exit()

        if policy == "greedy":
            self.policy = Greedy()

        elif policy == "eps_greedy":
            self.policy = EpsGreedy(eps=eps)

        elif policy == "eps_dec_greedy":
            self.policy = EpsDecGreedy(eps=eps, eps_min=eps_min, eps_decrease=eps_decrease)

        else:
            print("error:方策候補が見つかりませんでした")
            sys.exit()

        self.current_state = self.next_state = 0
        self.n_state = n_state

    def update(self, current_action, reward, i):

        current_state = self.current_state
        next_state = self.next_state
     
        
        if i == "Sarsa" or i == "Sarsa lambda" or i == "Watkin lambda" or i == "RS on" or i == "RS(λ) on" or i == "RS(λ)" or i == "RS tau on" or i == "RS tau off": 
            
            next_action = self.select_action_sarsa()
            self.value_func.update(current_state, current_action, reward, next_state, next_action)
        
        elif i == "Q Learning" or i == "RS"  :
            
            self.value_func.update(current_state, current_action, reward, next_state)

        else:
            pass

        # self.policy.update_params()

    def GRC_update(self, e_tmp):

        self.value_func.GRC_update(e_tmp)

    def select_action(self):
        return self.policy.select_action(self.value_func.get_value(), self.current_state)
    
    def select_action_sarsa(self):
        return self.policy.select_action(self.value_func.get_value(), self.next_state)

    """
    def print_value(self):
        f = open('Q_value_waka.csv','w')
        writer = csv.writer(f, lineterminator = '\n')
        writer.writerows(self.value_func.get_value())
        print(self.value_func.get_value())
        f.close()
    """

    def print_value(self):
        self.value_func.print_value()

    def init_params(self):
        self.value_func.init_params()
        self.policy.init_params()
        self.current_state = self.next_state = 0