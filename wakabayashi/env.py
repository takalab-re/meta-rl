import numpy as np
import matplotlib
import matplotlib.pyplot as plt

import seaborn as sns
import random
import sys
import csv

"""# 環境"""

class Environment(object):
    def __init__(self):
        self.current_state = 0
        self.next_state = 0
        self.reward = 0
        self.num_state = 0
        self.num_action = 0
        self.start_state = 0
        self.goal_state = 0

    def get_current_state(self):
        return self.current_state

    def get_next_state(self):
        return self.next_state

    def get_reward(self):
        return self.reward

    def get_num_state(self):
        return self.num_state

    def get_num_action(self):
        return self.num_action

    def get_start(self):
        return self.start

    def get_goal_state(self):
        return self.goal

"""# グリッドワールド"""

class GlidWorld(Environment):
    def __init__(self, height, length, start, goal):
        self.reward = 1
        self.num_state = height * length
        self.num_action = 4
        self.height = height
        self.length = length
        self.start = start
        self.goal = goal

    def to_s(self, row, col):
            return ((col * self.height) + row)

    def to_p(self, s):
        y = s // self.height
        x = s % self.height
        return x, y

    def move(self, state, action):
        UP = 0
        DOWN = 2
        LEFT = 3
        RIGHT = 1

        column, row = self.to_p(state)

        if action == UP:
            if (row) > 0:
                row -= 1
        elif action == DOWN:
            if (row) < (self.height - 1):
                row += 1
        elif action == LEFT:
            if (column) > 0:
                column -= 1
        elif action == RIGHT:
            if (column) < (self.length - 1):
                column += 1

        new_state = self.to_s(column, row)
        reward = self.get_reward(new_state)

        return new_state, reward

    def get_reward(self, state):

        if state == self.goal:
            return 1

        else:
            return -1

"""# 崖ありグリッドワールド"""

class Cliff_GlidWorld(GlidWorld):
    def __init__(self, height, length, start, goal, cliff):
        self.reward = 1
        self.num_state = height * length
        self.num_action = 4
        self.height = height
        self.length = length
        self.start = start
        self.goal = goal
        self.cliff = cliff

    def to_s(self, row, col):
        return ((row * self.height) + col)

    def to_p(self, s):
        y = s // self.height
        x = s % self.height
        return x, y

    def move(self, state, action):
        UP = 0
        DOWN = 1
        LEFT = 2
        RIGHT = 3

        column, row = self.to_p(state)

        if action == UP:
            if (row) > 0:
                row -= 1
        elif action == DOWN:
            if (row) < (self.height - 1):
                row += 1
        elif action == LEFT:
            if (column) > 0:
                column -= 1
        elif action == RIGHT:
            if (column) < (self.length - 1):
                column += 1

        new_state = self.to_s(column, row)
        reward = self.get_reward(new_state)

        return new_state, reward

    def get_reward(self, state):

        if state == self.goal:
            return 1
        elif state in self.cliff:
            return -1
        else:
            return 0

"""# suboptima"""

class Cliff_optima_GlidWorld(GlidWorld):
    def __init__(self, height, length, start, goal, cliff):

        # self.reward = [1,8,2,7,4,5,3,6]
        self.reward = [8,1,5,4,7,2,6,3]
        # self.reward = [8,0,0,0,0,0,0,0]
        self.num_state = height * length
        self.num_action = 4
        self.height = height
        self.length = length
        self.start = start
        self.goal = goal
        self.cliff = cliff


    def to_s(self, row, col):
        return ((col * self.height) + row)

    def to_p(self, s):
        y = s // self.height
        x = s % self.height
        return x, y

    def move(self, state, action):
        UP = 0
        DOWN = 2
        LEFT = 3
        RIGHT = 1

        column, row = self.to_p(state)

        if action == UP:
            if (row) > 0:
                row -= 1
        elif action == DOWN:
            if (row) < (self.height - 1):
                row += 1
        elif action == LEFT:
            if (column) > 0:
                column -= 1
        elif action == RIGHT:
            if (column) < (self.length - 1):
                column += 1

        new_state = self.to_s(column, row)
        reward = self.get_reward(new_state)

        return new_state, reward

    def get_reward(self, state):

        if state in self.goal:
            return self.reward[self.goal.index(state)]
        #elif state in self.cliff:
        #    return -1
        else:
            return 0