import numpy as np
import matplotlib
matplotlib.use('Agg') 
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_palette("husl", n_colors = 8)
# sns.set()

sns.set_context("notebook", font_scale = 1)
import random
import sys
import csv

from env import GlidWorld, Cliff_GlidWorld, Cliff_optima_GlidWorld
from agent import Agent
from policy import TestGreedy, Greedy, EpsGreedy, EpsDecGreedy, Q_Learning, Sarsa, Sarsa_lambda, Watkin_lambda, RS_off, RS_on, RS_lam_off, RS_lam_on, RS_tau_on
# from imgprinter import Heat_Map

"""# メイン関数"""

class Heat_Map():
    def print_map(value_func, Q_value):
        Z = np.zeros(81)
        for i in range(81):
            Z[i] = (max(Q_value[i]))
        Z = np.array(Z).reshape(9,9)
        # print(Q_value)
        # print("Z")
        # print(Z)
        sns.heatmap(Z)

    def print_heat(tmp, w_x, w_y, all_action, log=False):
        if all_action:
            heat = np.zeros([3*w_x, 3*w_y])
            for x in range(w_x):
                for y in range(w_y):
                    state = x * w_y + y
                    outside = tmp[state].mean()
                    #outside = 0
                    center = tmp[state].max()
                    #center = 0
                    heat[3*x][3*y] = outside
                    heat[3*x][3*y+1] = tmp[state][0]
                    heat[3*x][3*y+2] = outside
                    heat[3*x+1][3*y] = tmp[state][3]
                    heat[3*x+1][3*y+1] = center
                    heat[3*x+1][3*y+2] = tmp[state][1]
                    heat[3*x+2][3*y] = outside
                    heat[3*x+2][3*y+1] = tmp[state][2]
                    heat[3*x+2][3*y+2] = outside
        else:
            heat = np.zeros([w_x, w_y])
            for x in range(w_x):
                for y in range(w_y):
                    state = x * w_y + y
                    heat[x][y] = max(tmp[state])
        fig = plt.subplots(figsize=(20, 6))
        if log == True:
            ##############################################
            data = heat + 1
            log_norm = LogNorm(vmin=data.min().min(), vmax=data.max().max())
            cbar_ticks = [math.pow(10, i) for i in range(math.floor(math.log10(data.min().min())), 1+math.ceil(math.log10(data.max().max())))]
            HEAT = sns.heatmap(
                data,
                norm=log_norm,
                cbar_kws={"ticks": cbar_ticks}, 
                cmap= "viridis", square=True
                ,linewidths=.5
            )
            return HEAT
            ################################################
        else:
            HEAT = sns.heatmap(heat, annot=False, cbar=True, square=True, cmap= "viridis")
            return HEAT

def hairetu(data, EPISODE_NUM, reward_num, j, ax):
    goal1 = np.zeros(EPISODE_NUM)
    goal2 = np.zeros(EPISODE_NUM)
    goal3 = np.zeros(EPISODE_NUM)
    goal4 = np.zeros(EPISODE_NUM)
    goal5 = np.zeros(EPISODE_NUM)
    goal6 = np.zeros(EPISODE_NUM)
    goal7 = np.zeros(EPISODE_NUM)
    goal8 = np.zeros(EPISODE_NUM)

    select  = np.zeros((reward_num, EPISODE_NUM))


    reward_dict = {i:0 for i in range(reward_num)}
    episode_dict = {i:0 for i in range(EPISODE_NUM)}


    for i in range(EPISODE_NUM):
        goal1[i] = data[i][0]
        goal2[i] = data[i][1]
        goal3[i] = data[i][2]
        goal4[i] = data[i][3]
        goal5[i] = data[i][4]
        goal6[i] = data[i][5]
        goal7[i] = data[i][6]
        goal8[i] = data[i][7]



    plotter(goal8, "goal8", ax)
    plotter(goal7, "goal7", ax)
    plotter(goal6, "goal6", ax)
    plotter(goal5, "goal5", ax)
    plotter(goal4, "goal4", ax)
    plotter(goal3, "goal3", ax)
    plotter(goal2, "goal2", ax)
    plotter(goal1, "goal1", ax)


    # current_palette = sns.color_palette(n_colors=24)
    # sns.set_palette("husl",8)
    
    plt.subplots_adjust(right = 0.8)
    ax.legend(bbox_to_anchor=(1.01, 0.5), loc='upper left', borderaxespad=0, fontsize ='small', frameon=False)


def plotter(data, l, ax):
    ax.plot(data, label = l)
    # ax.set_title("200 times average selection")
    # ax.legend(bbox_to_anchor=(0, ),fontsize ='small')
    ax.set_xlabel("episode")
    ax.set_ylabel("propotion of chosen goals")
    plt.savefig("selectivity.pdf", dpi=500)

        

goal1 = [2,6,26,62,78,74,54,18]
goal2 = [26,62,78,74,54,18,2,6]
goal3 = [78,74,54,18,2,6,26,62]
goal4 = [54,18,2,6,26,62,78,74]
# GOAL = []
# GOAL.append(str(goal1))
# GOAL.append(str(goal2))
# GOAL.append(str(goal3))
# GOAL.append(str(goal4))
cliff = [27,28,29,30,32,48,50]
# env = GlidWorld(5, 5, 0, 24)
#env = Cliff_GlidWorld(7, 7, 42, 48, cliff)
env = Cliff_optima_GlidWorld(9, 9, 40, goal1, cliff)


SIMULATION_NUM = 100

EPISODE_NUM = 500
STEP_NUM = 100

#アニメーション用配列
ims = []    
heat = []

# GOAL = goal
# RS(Rg、zeta)
reward_RS = "RS"
reward_RS_onpolicy = "RS on"
reward_RS_on_eligibility = "RS(λ) on"
reward_RS_off_eligibility = "RS(λ)"
RS_tau_on = "RS tau on"
RS_tau_off = "RS tau off"





agents = {}

# agents.update({"Q Learning": Agent(
#     policy="eps_dec_greedy",
#     eps=1.0,
#     eps_min=0.0,
#     eps_decrease=0.001,
#     n_state=env.get_num_state(),
#     n_action=env.get_num_action()
# )})

# agents.update({"Sarsa": Agent(
#     value_func = "Sarsa",
#     policy="eps_dec_greedy",
#     eps=1.0,
#     eps_min=0.0,
#     eps_decrease=0.001,
#     n_state=env.get_num_state(),
#     n_action=env.get_num_action()
# )})

# agents.update({"Sarsa lambda": Agent(
#     value_func="Sarsa_lambda",
#     policy="eps_dec_greedy",
#     eps=1.0,
#     eps_min=0.0,
#     eps_decrease=0.001,
#     lamda = 0.9,
#     n_state=env.get_num_state(),
#     n_action=env.get_num_action()
# )})

# agents.update({"Watkin lambda": Agent(
#     value_func="watkin",
#     policy="eps_dec_greedy",
#     eps=1.0,
#     lamda=0.9,
#     eps_min=0.0,
#     eps_decrease=0.001,
#     n_state=env.get_num_state(),
#     n_action=env.get_num_action()
# )})

# agents.update({reward_RS: Agent(
#     value_func="RS",
#     n_state=env.get_num_state(),
#     n_action=env.get_num_action(),
#     r_g = 7.5,
#     zeta = 1
# )})

agents.update({reward_RS_off_eligibility: Agent(
    value_func="RS_off_eligibility",
    n_state=env.get_num_state(),
    n_action=env.get_num_action(),
    r_g = 7.5,
    zeta = 1
)})

# agents.update({reward_RS_onpolicy: Agent(
#     value_func="RS_onpolicy",
#     n_state=env.get_num_state(),
#     n_action=env.get_num_action(),
#     r_g = 7.5,
#     zeta = 1
# )})

# agents.update({reward_RS_on_eligibility: Agent(
#     value_func="RS_on_eligibility",
#     n_state=env.get_num_state(),
#     n_action=env.get_num_action(),
#     r_g = 7.5,
#     zeta = 1
# )})

# agents.update({RS_tau_on: Agent(
#     value_func="RS_tau_on",
#     n_state=env.get_num_state(),
#     n_action=env.get_num_action(),
#     r_g = 7.5,
#     zeta = 1
# )})

# agents.update({RS_tau_off: Agent(
#     value_func="RS_tau_off",
#     n_state=env.get_num_state(),
#     n_action=env.get_num_action(),
#     r_g = 7.5,
#     zeta = 1
# )})


reward_num = 8
reward_given = np.zeros((SIMULATION_NUM, EPISODE_NUM))
#照準の報酬地
reward_sort = np.zeros(reward_num)
for i in range(reward_num):
    reward_sort[i] = i
#指標のグラフ
reward_index = np.zeros(EPISODE_NUM)
#選択確率グラフ
reward_prob = np.zeros((EPISODE_NUM, reward_num))
#辞書型で選択した報酬を合計するやつ
select_graph = {i:0 for i in range(reward_num)}
#辞書型を二次元配列。エピソードごと
select_epi = {i:0 for i in range(EPISODE_NUM)}
#エピソード毎報酬グラフ
every_reward = np.zeros(EPISODE_NUM) 
#報酬遷移グラフ
reward_graph = np.zeros(EPISODE_NUM)
#ステップ枚報酬グラフ
step_graph = np.zeros(EPISODE_NUM)
#Eg推移グラフ
Eg_graph = np.zeros(EPISODE_NUM)
#推定方策のみグラフ
greedy_Eg_graph = np.zeros(EPISODE_NUM)

# reward_fig = plt.figure()
print("start training")
agents_select = {}
agents_reward_prob = {}
agents_steps = {}
agents_rewards = {}
agents_Eg = {}
agents_Greedy_Eg = {}
Rg = np.full(EPISODE_NUM,7.5)
fig = plt.figure()
fig.suptitle("500 times average selection")
counter = 1

for i in agents.keys():
    ax = fig.add_subplot(2,1,counter)
    print("{}'s training".format(i))
    step_graph = np.zeros(EPISODE_NUM)
    reward_graph = np.zeros(EPISODE_NUM)
    # goal8_graph = np.zeros(EPISODE_NUM)
    Eg_graph = np.zeros(EPISODE_NUM)
    greedy_Eg_graph = np.zeros(EPISODE_NUM)
    select_graph = {i:0 for i in range(reward_num)}
    reward_prob = np.zeros((EPISODE_NUM, reward_num))
    select_epi = {i:0 for i in range(EPISODE_NUM)}
    #学習開始
    reward_index = np.zeros(EPISODE_NUM)
    for sim in range(SIMULATION_NUM):
        
        agents[i].init_params()
        
        every_reward = np.zeros(EPISODE_NUM)
        env.goal = goal1
        G = goal1
        sum_goal8 = 0

        for epi in range(EPISODE_NUM):
            reward_select = np.zeros(reward_num)
            sum_reward = 0
            agents[i].current_state = env.get_start()
            ##############推定方策を確認するため##################################

            # GreedyEg = 0
            # Ng = 0
            # for step in range(STEP_NUM):
            #     action = TestGreedy.select_action(agents[i].value_func.Q, agents[i].current_state)
            #     agents[i].next_state, reward = env.move(agents[i].current_state, action)
            #     sum_reward += reward
            #     agents[i].current_state = agents[i].next_state
            #     if agents[i].current_state in G or step == STEP_NUM:
            #         break
            
            # GreedyEg = (sum_reward + 0.9 * (Ng * GreedyEg)) / float(1 + 0.9 * Ng)
            # Ng = 1.0 + 0.9 * Ng

            # greedy_Eg_graph[epi] += GreedyEg
            # # ######################################################################
            #ステップ開始
            sum_reward = 0
            ##########報酬位置変更##################################################
            # if epi == 500 :
            #     env.goal = goal2
            #     G = goal2
            # if epi == 1000:
            #     env.goal = goal3
            #     G = goal3
            # if epi == 1500:
            #     env.goal = goal4
            #     G = goal4
            # if epi == 2000:
            #     env.goal = goal1
            #     G = goal1
            # if epi == 2500 :
            #     env.goal = goal2
            #     G = goal2
            # if epi == 3000:
            #     env.goal = goal3
            #     G = goal3
            # if epi == 3500:
            #     env.goal = goal4
            #     G = goal4
            # if epi == 4000:
            #     env.goal = goal1
            #     G = goal1
            # else:
            #     pass
            #########################################################################
            agents[i].current_state = env.get_start()
            ################学習開始#####################################################
            for step in range(STEP_NUM):
                action = agents[i].select_action()
                agents[i].next_state, reward = env.move(agents[i].current_state, action)
                
                sum_reward += reward
                
                agents[i].update(action, reward, i)

                ##############エージェントの位置を描画できるやつ################
                # print("epi", end = " ")
                # print(epi)
                # if epi == 60 :
                #     im = Grid_Map.print_anim(9,9,agents[i].current_state, epi)
                #     ims.append([im])
                ################################################################
                agents[i].current_state = agents[i].next_state
                # print("reward: ",end = " ")
                # print(reward)
                if agents[i].current_state in G or step == STEP_NUM:
                # if agents[i].current_state == 24 or step == STEP_NUM:
    
                    # if epi > 2999:
                    # print("reward: ",end = " ")
                    # print(reward)
                    #     print("Eg: ",end = " ")
                    #     print(agents[i].value_func.e_g)
                    # print(step)
                    # print("epi" + str(epi))
                    # print("reward: "+ str(reward))
                    select_graph[reward-1] += 1
                    reward_select[sum_reward-1] += 1
                    reward_given[sim][epi] = reward
                    

                    break
            agents[i].policy.update_params()
    
            #RSの時のみGRCの更新
            if  i == reward_RS or i == reward_RS_on_eligibility or i == reward_RS_off_eligibility  or i == reward_RS_onpolicy or i == RS_tau_on or i == RS_tau_off:
                agents[i].GRC_update(sum_reward)
                # agents[i].GRC_update(sum_reward / float(step + 1))
            else:
                pass
            agents[i].value_func.Q.shape
            ################エピソード1での確認########################################################
            # if epi == 0:
                # print(agents[i].value_func.Q)
                
            ###############逐次的にヒートマップ出力########################################################
            if sim == 0:
                # if epi > 150 and epi < 300:
            # if epi < 10:
            # if epi == 99 or epi == 199:
        ######Q値ヒートマップ########
                # h = Heat_Map.print_heat(agents[i].value_func.Q, 9, 9, all_action=True, log=False)
        #         plt.title("epi"+str(epi))
        #         plt.tight_layout()
                # plt.savefig("RS(λ)tau/ " + str(i)+ " " + str(epi) + " " + str(sim) +  " Q value EPISODE.png")
                # plt.close()
        #         #######RS値ヒートマップ#######
        #         h = Heat_Map.print_heat(agents[i].value_func.eligibility ,9,9 , all_action=True, log=False)
        #         plt.title("epi"+str(epi))
        #         plt.tight_layout()
        #         plt.savefig("RS(λ)tau/ " + str(i)+ " " + str(epi) + " " + str(sim) +  " eligibility value EPISODE.png")
        #         plt.close()
        #         #####tau値ヒートマップ######
                # h = Heat_Map.print_heat((agents[i].value_func.rs) ,9,9 , all_action=True, log=False)
                h = Heat_Map.print_heat((agents[i].value_func.tau) ,9,9 , all_action=True, log=False)
        # # #     #         # heat.append([h])
                plt.title("episode "+str(epi+1))
                plt.axis("off")
                plt.tight_layout()
                plt.savefig("tau/ " + str(i)+ " " + str(epi+1) + " " + str(sim) +  " τ value EPISODE.png")
                plt.close()
        #         # fig.show()

            # print(agents[i].value_func.Q)
            #############################################################################################
            # reward_select = reward_select/EPISODE_NUM

            #選択確率グラフの更新
            #そのエピソードで得た報酬に+1された配列を追加
            reward_prob[epi] += reward_select
            # print("rearad")s
            #今までの合計をargsortし、それをsortに代入
            sort = np.argsort(reward_prob[epi])
            # print("sortbe")
            # print(sort)
            #実際の報酬値と差をとル
            sort = sort - reward_sort
            # print("sortaf")
            # print(sort)
            # print("Sum")
            # print(sum(abs(sort)))
            #差の合計を配列追加
            reward_index[epi] += sum(abs(sort))
            

            #選択確率評価のグラフの更新
            select_epi[epi] = select_graph
            # print(select_epi)
            # print("reward_prob")
            # print(reward_prob)
           
            reward_graph[epi] += sum_reward
            
    
            step_graph[epi] += step
            ###########毎ステップ収益グラフ##############
            # every_reward[epi] += sum_reward
            ###########Egを表示グラフ######################
            # Eg = agents[i].value_func.e_g
            # Eg_graph[epi] += Eg
        
        
        #########毎エピソードのしゅつりょく#####################################################
        
        # plt.plot(every_reward, alpha = 0.5)
        # plt.tight_layout()
        # plt.title("every reward")
        # plt.show()

        ##########################################################################################################
        # plt.plot(reward_index/SIMULATION_NUM, label = sim)
        # plt.title(sim)
        # plt.savefig("sim"+str(sim)+".png")
        # plt.show()


        selectivity=reward_prob/SIMULATION_NUM
    #############グラフのデータ保存###################################
    # agents_Greedy_Eg.update({i: greedy_Eg_graph})
    # agents_Eg.update({i:Eg_graph})

    # print(reward_prob/SIMULATION_NUM)
    # plt.title(sim)
    # plt.savefig("sim.png")
    selectivity=reward_prob/SIMULATION_NUM
    



    hairetu(selectivity, EPISODE_NUM, reward_num, i, ax)
    counter += 1
    agents_select.update({i:reward_index})
    agents_steps.update({i:step_graph})
    agents_rewards.update({i: reward_graph})
    agents_reward_prob.update({i: selectivity})
    #########################################################################################

    # plt.tight_layout()
    # plt.title("every reward")
    # plt.savefig("Every reward.png")
    # plt.show()
    # print(i)
    # print_heat(agents[i].value_func.Q,9,9,all_action=True)
    #agents[i].print_value()
    # Arrow_Map.print_q_direction(agents[i].value_func.Q,9,9)
    # print_heat(agents[i].value_func.Q,9,9)
    # print_heat(agents[i].value_func.Q,9,9,all_action=True)
    # print("Tau: ",end = " ")
    # print((agents[i].value_func.t_curr)+(agents[i].value_func.t_post))

    print("finish")

# print(env.goal)
# with open('data.csv', 'w') as f:
#     writer = csv.writer(f)
#     writer.writerows([reward_given])

###########選択確率########################################
# print("選択確率")
# print(reward_prob)

# # グラフ作成/
# reward_prob_fig = plt.figure()
# cx = reward_prob_fig.add_subplot(111)
# reward = np.array([1,2,3,4,5,6,7,8])
# for i in agents.keys():
#     # cx.bar(reward, agents_reward_prob[i]/ 100000)

# cx.legend()  # 凡例を付ける
# plt.ylim(4, 8)
# cx.set_title("Reward Selection Probability")  # グラフタイトルを付ける
# cx.set_ylim(0.0,1.0)
# cx.set_xlabel("Reward")  # x軸のラベルを付ける
# cx.set_ylabel("Probability")  # y軸のラベルを付ける
    
# reward_prob_fig.tight_layout()
# plt.savefig("Reward_Prob(aleph = 5.5).png")
# plt.show()
###########csv出力###################################

###########選択確率評価########################################
# selected_fig = plt.figure()
# ax = selected_fig.add_subplot(111)
# for i in agents.keys():
# #     x = agents_reward_prob[i]
# #     select_point = np.zeros(EPISODE_NUM)
# #     sorted = np.argsort(x)
# #     for j in range(EPISODE_NUM):
# #         print("sorted: ")
# #         print(sorted[j])

# #         sorted[j] = sorted[j] - reward_sort
# #         select_point[j] = sum(abs(sorted[j]))
# #         print("fin: " )
# #         print(select_point[j])
    
#     if i == "RS":
#         ax.plot(agents_select[i]/SIMULATION_NUM, label = i, color =  "#030bfc")
#     elif i == "RS(λ)":
#         ax.plot(agents_select[i]/SIMULATION_NUM, label = i, color = "#fc0303")
#     # ax.plot(select_point, label = i)

#     ax.legend()  # 凡例を付ける

#     ax.set_title("goal selectivity evaluation")  # グラフタイトルを付ける
#     ax.set_xlabel("episode")  # x軸のラベルを付ける
#     ax.set_ylabel("Evaluated value")  # y軸のラベルを付ける
#     plt.savefig("evaluation.pdf")

##########################################################


###########結果を出力########################################

##################################################################
#選択確率グラフ

reward_fig = plt.figure()
# reward_fig.suptitle(r"500 times average $E_G$")

ax = reward_fig.add_subplot(1,1,1)
# lt.subplots_adjust(right = 0.8)

for i in agents.keys():
    # if i == "RS":
#         # ax.plot(agents_reward_prob[i], label = i)
        # ax.plot(agents_rewards[i]/SIMULATION_NUM, label=i, color =  "#030bfc")
    # elif i == "RS(λ)":
        # ax.plot(agents_Eg[i]/SIMULATION_NUM, label=i, color = "#fc0303")
        # ax.plot(agents_rewards[i]/SIMULATION_NUM, label=i, color = "#fc0303")
    # else:
        ax.plot(agents_rewards[i]/SIMULATION_NUM, label=i)
        # ax.plot(agents_Greedy_Eg[i] / SIMULATION_NUM, label="Q-Greedy", color = "#000000")
# ax.plot(Rg, linestyle='--', color = '#000000')

ax.legend()  # 凡例を付ける
#plt.ylim(4, 8)
ax.set_title("Average reward over 500 simulations")
# ax.set_title(r"Average $E_G$ over 500 simulations")  # グラフタイトルを付ける

ax.set_xlabel("episode")  # x軸のラベルを付ける
# ax.set_ylabel(r"$E_G$")  # y軸のラベルを付ける
ax.set_ylabel("reward")
reward_fig.tight_layout()
# plt.show() 
plt.savefig("img/reward.png", dpi = 600)
# # ##################################################################


##################################################################
#Eg、もしくは報酬グラフ

# Eg_fig = plt.figure()

# bx = Eg_fig.add_subplot(111)
# bx = reward_fig.add_subplot(2,1,2)


# for i in agents.keys():
#     # if i == "RS(λ)":
#         bx.plot(agents_rewards[i] / SIMULATION_NUM, label=i)
# #         bx.plot(agents_Eg[i] / SIMULATION_NUM, label=i, color = "#fc0303")
# #         bx.plot(agents_Greedy_Eg[i] / SIMULATION_NUM, label="Q-Greedy", color = "#000000")
# # bx.plot(Rg, linestyle='--', color = '#000000')
# bx.legend()  # 凡例を付ける
# #plt.ylim(4, 8)
# bx.set_title("500 times average Reward")  # グラフタイトルを付ける
# bx.set_xlabel("episode")  # x軸のラベルを付ける
# bx.set_ylabel(r"$E_G$")  # y軸のラベルを付ける
# reward_fig.tight_layout()
# plt.savefig("img/Eg.pdf", dpi =600)

# plt.show()  # グラフを表示
##################################################################

##################################################################


step_fig = plt.figure()
cx = step_fig.add_subplot(111)
for i in agents.keys():
    if i == "RS":
        # ax.plot(agents_reward_prob[i], label = i)
        cx.plot(agents_steps[i]/SIMULATION_NUM,  label=i, color = "#030bfc")
    if i == "RS(λ)":
        cx.plot(agents_steps[i]/SIMULATION_NUM, label=i, color = "#fc0303")

cx.legend()  # 凡例を付ける
cx.set_title("average steps per episode over 500 simulations")  # グラフタイトルを付ける
cx.set_xlabel("episode")  # x軸のラベルを付ける
cx.set_ylabel("steps per episode")  # y軸のラベルを付ける
# cx.show()  # グラフを表示
plt.savefig("img/Step.pdf", dpi=600)

##################################################################

"""リスクを選んでいるのをわかっているのか？
多少確率をやっている人などの

*   エピソード75〜100で急に降下している
*   それまでは平均的に6をいた。
*   ヒートマップをみてみると、3ステップ以上離れている場所のQ値は大体同じになってしまっているから、そこまでつかないといけない
*   
*   リスト項目


*   6に立て続けて行動をしていて、収益が降下
*   ただ満足をしていないので、探索をする

#一番下
"""