
import numpy as np
import matplotlib
matplotlib.use('Agg') 
import matplotlib.pyplot as plt

import seaborn as sns
import random
import sys
import csv

class TestGreedy(object):
    def select_action(value, state):
        idx = np.where(value[state] == max(value[state]))
        return random.choice(idx[0])

class Greedy(object):
    def select_action(self, value, state):
        idx = np.where(value[state] == max(value[state]))
        return random.choice(idx[0])

    def init_params(self):
        pass

    def update_params(self):
        pass

"""# ε- greedy"""

class EpsGreedy(Greedy):
    def __init__(self, eps = 0.3):
        self.eps = eps

    def select_action(self, value, state):
        if random.random() < self.eps:
            return random.choice(range(len(value[state])))
        else:
            return super().select_action(value, state)

    def init_params(self):
        pass

    def update_params(self):
        pass

"""#  dec ε- greedy"""

class EpsDecGreedy(Greedy):
    def __init__(self, eps = 1.0, eps_min = 0.0, eps_decrease = 0.05):
        self.eps = self.eps_init = eps
        self.eps_min = eps_min
        self.eps_decrease = eps_decrease

    def select_action(self, value, state):
        if random.random() < self.eps:
            return random.choice(range(len(value[state])))
        else:
            return super().select_action(value, state)

    def init_params(self):
        self.eps = self.eps_init

    def update_params(self):
        self.eps = max(self.eps - self.eps_decrease, self.eps_min)

"""# Q-Learning"""

class Q_Learning(object):
    def __init__(self, num_state, num_action, learning_rate = 0.1, discount_rate = 0.9):
        self.learning_rate = learning_rate
        self.discount_rate = discount_rate
        self.num_state = num_state
        self.num_action = num_action

        self.Q = np.zeros((self.num_state, self.num_action))
        self.estimate_policy = Greedy() # 推定方策

    def update(self, current_state, current_action, reward, next_state):
        alpha = self.learning_rate
        gamma = self.discount_rate

        next_action = self.estimate_policy.select_action(self.Q, next_state)
        TD_error = reward + gamma * self.Q[next_state, next_action] - self.Q[current_state, current_action]
        self.Q[current_state, current_action] += alpha * TD_error

        return current_state, next_state, next_action

    def GRC_update(self, e_tmp):
        pass

    def init_params(self):
        self.Q = np.zeros((self.num_state, self.num_action))

    def get_value(self):
        return self.Q

    def print_value(self):
        print(self.Q)

"""# Sarsa"""

class Sarsa(object):
    def __init__(self, num_state, num_action, learning_rate = 0.1, discount_rate = 0.9):
        self.learning_rate = learning_rate
        self.discount_rate = discount_rate
        self.num_state = num_state
        self.num_action = num_action
        self.estimate_policy = Greedy() # 推定方策

        self.Q = np.zeros((self.num_state, self.num_action))

    def update(self, current_state, current_action, reward, next_state, next_action):
        alpha = self.learning_rate
        gamma = self.discount_rate

        TD_error = reward + gamma * self.Q[next_state, next_action] - self.Q[current_state, current_action]
        self.Q[current_state, current_action] += alpha * TD_error

        return current_state, next_state, next_action

    def GRC_update(self, e_tmp):
        pass

    def init_params(self):
        self.Q = np.zeros((self.num_state, self.num_action))

    def get_value(self):
        return self.Q

    def print_value(self):
        print(self.Q)

"""# Sarsa Lambda"""

class Sarsa_lambda(Q_Learning):
    def __init__(self, num_state, num_action, lamda = 0.9, learning_rate = 0.1, discount_rate = 0.9):
        self.learning_rate = learning_rate
        self.discount_rate = discount_rate
        self.num_state = num_state
        self.num_action = num_action
        self.lamda = lamda
        self.estimate_policy = Greedy() # 推定方策
        self.eligibility = np.zeros((self.num_state, self.num_action))

    def update(self, current_state, current_action, reward, next_state, next_action):
        alpha = self.learning_rate
        gamma = self.discount_rate

        TD_error = reward + gamma * self.Q[next_state, next_action] - self.Q[current_state, current_action]

        self.eligibility[current_state, current_action] += 1

        self.Q += alpha * TD_error * self.eligibility
        self.eligibility = gamma * 0.9 * self.eligibility


        return current_state, next_state, next_action, self.eligibility

    def GRC_update(self, e_tmp):
        pass

    def init_params(self):

        self.Q = np.zeros((self.num_state, self.num_action))
        self.eligibility = np.zeros((self.num_state, self.num_action))

    def print_value(self):
        print(self.eligibility)

"""# Watkin Lambda"""

class Watkin_lambda(Q_Learning):
    def __init__(self, num_state, num_action, lamda = 0.9, learning_rate = 0.1, discount_rate = 0.9):
        self.learning_rate = learning_rate
        self.discount_rate = discount_rate
        self.num_state = num_state
        self.num_action = num_action
        self.lamda = lamda
        self.itit_lamda = lamda

        self.Q = np.zeros((self.num_state, self.num_action))
        self.estimate_policy = Greedy()# 推定方策
        self.eligibility = np.zeros((self.num_state, self.num_action))

    def update(self, current_state, current_action, reward, next_state, true_next_action):
        alpha = self.learning_rate
        gamma = self.discount_rate

        next_action = self.estimate_policy.select_action(self.Q, next_state)

        TD_error = reward + gamma * self.Q[next_state, next_action] - self.Q[current_state, current_action]

        self.eligibility[current_state, current_action] += 1
        
        self.Q += alpha * TD_error * self.eligibility

        if next_action == true_next_action:
            self.eligibility= gamma * 0.9 * self.eligibility
        else:
            self.eligibility = np.zeros((self.num_state, self.num_action))

        return current_state, next_state, true_next_action, self.eligibility

    def GRC_update(self, e_tmp):
        pass

    def init_params(self):

        self.Q = np.zeros((self.num_state, self.num_action))
        self.eligibility = np.zeros((self.num_state, self.num_action))
        self.lamda = self.itit_lamda

    def print_value(self):
        print(self.eligibility)

"""# RS + GRC(off policy)"""

class RS_off(Q_Learning):
    def __init__(self, num_state, num_action, learning_rate=0.1, discount_rate=0.9, r_g=7.5, zeta=0.05):

        super().__init__(num_state, num_action, learning_rate, discount_rate)

        self.r = []
        self.t_curr = []
        self.t_post = []
        self.rs = []
        self.zeta = zeta
        self.r_g = r_g
        self.gamma_g = 0.9
        self.e_g = 0
        self.n_g = 0
        self.sum_Eg = []
        self.tau = 0

    def update(self, current_state, current_action, reward, next_state):
        alpha_tau = self.learning_rate
        gamma_tau = self.discount_rate

        # Q 値の更新を行う
        q_current_state, q_next_state, next_action = super().update(current_state, current_action, reward, next_state)

        # tau 値の更新
        t = self.t_curr + self.t_post
        # self.t_curr[q_current_state, current_action] += 1
        if reward == 0:
            self.t_curr[q_current_state, current_action] += 0
        else:
            self.t_curr[q_current_state, current_action] += 1
        
        self.t_post[q_current_state, current_action] += alpha_tau * (gamma_tau * t[q_next_state, next_action] - self.t_post[q_current_state, current_action])
        t = self.t_curr + self.t_post
        self.tau = t


        # 基準値 R の更新
        delta_g = min(self.e_g - self.r_g, 0)
        a = self.estimate_policy.select_action(self.Q, q_current_state)
        self.r[q_current_state] = self.Q[q_current_state][a] - self.zeta * delta_g

        # RS 値の更新
        self.rs = t * (self.Q.T - self.r).T

    def GRC_update(self, e_tmp):
        self.e_g = (e_tmp + self.gamma_g * (self.n_g * self.e_g)) / float(1 + self.gamma_g * self.n_g)
        # self.e_g = e_tmp
        self.sum_Eg.append(self.e_g)
        
        self.n_g = 1.0 + self.gamma_g * self.n_g

    def init_params(self):
        super().init_params()

        self.t_curr = np.zeros([self.num_state, self.num_action])
        self.t_post = np.zeros([self.num_state, self.num_action])
        self.rs = np.zeros([self.num_state, self.num_action])
        self.r = np.zeros(self.num_state)
        self.e_g = 0

    def get_value(self):
        return self.rs

"""# RS + GRC(on policy)"""

class RS_on(Sarsa):
    def __init__(self, num_state, num_action, learning_rate=0.1, discount_rate=0.9, r_g=7.5, zeta=0.05):

        super().__init__(num_state, num_action, learning_rate, discount_rate)

        self.r = []
        self.t_curr = []
        self.t_post = []
        self.rs = []
        self.zeta = zeta
        self.r_g = r_g
        self.gamma_g = 0.9
        self.e_g = 0
        self.n_g = 0
        self.sum_Eg = []
        self.tau = 0

    def update(self, current_state, current_action, reward, next_state, next_action):
        alpha_tau = self.learning_rate
        gamma_tau = self.discount_rate

        # Q 値の更新を行う
        q_current_state, q_next_state, next_action = super().update(current_state, current_action, reward, next_state, next_action)

        # tau 値の更新
        t = self.t_curr + self.t_post
        self.t_curr[q_current_state, current_action] += 1
        self.t_post[q_current_state, current_action] += alpha_tau * (gamma_tau * t[q_next_state, next_action] - self.t_post[q_current_state, current_action])
        t = self.t_curr + self.t_post
        self.tau = t

        # 基準値 R の更新
        delta_g = min(self.e_g - self.r_g, 0)
        a = self.estimate_policy.select_action(self.Q, q_current_state)
        self.r[q_current_state] = self.Q[q_current_state][a] - self.zeta * delta_g

        # RS 値の更新
        self.rs = t * (self.Q.T - self.r).T

    def GRC_update(self, e_tmp):
        self.e_g = (e_tmp + self.gamma_g * (self.n_g * self.e_g)) / float(1 + self.gamma_g * self.n_g)
        # self.e_g = e_tmp
        self.n_g = 1.0 + self.gamma_g * self.n_g
        self.sum_Eg.append(self.e_g)

    def init_params(self):
        super().init_params()

        self.t_curr = np.zeros([self.num_state, self.num_action])
        self.t_post = np.zeros([self.num_state, self.num_action])
        self.rs = np.zeros([self.num_state, self.num_action])
        self.r = np.zeros(self.num_state)
        self.e_g = 0

    def get_value(self):
        return self.rs

"""# RS + GRC + 適格度トレース(off policy)"""

class RS_lam_off(Watkin_lambda):
    def __init__(self, num_state, num_action, learning_rate=0.1, discount_rate=0.9, r_g=0.75, zeta=0.05):

        super().__init__(num_state, num_action, learning_rate, discount_rate)

        self.r = []
        self.t_curr = []
        self.t_post = []
        self.rs = []
        self.zeta = zeta
        self.r_g = r_g
        self.gamma_g = 0.9
        self.e_g = 0
        self.n_g = 0
        self.sum_Eg = []
        self.tau = 0

    def update(self, current_state, current_action, reward, next_state, next_action):
        alpha_tau = self.learning_rate
        gamma_tau = self.discount_rate

        # Q 値の更新を行う
        q_current_state, q_next_state, next_action , eligibility = super().update(current_state, current_action, reward, next_state, next_action)

        # tau 値の更新
        t = self.t_curr + self.t_post
        # self.t_curr[q_current_state, current_action] += 1
        if reward == 0:
            self.t_curr[q_current_state, current_action] += 0
        else:
            self.t_curr[q_current_state, current_action] += 1
        self.t_post[q_current_state, current_action] += alpha_tau * (gamma_tau * t[q_next_state, next_action] - self.t_post[q_current_state, current_action])
        t = self.t_curr + self.t_post
        self.tau = t

        # 基準値 R の更新
        delta_g = min(self.e_g - self.r_g, 0)
        a = self.estimate_policy.select_action(self.Q, q_current_state)
        self.r[q_current_state] = self.Q[q_current_state][a] - self.zeta * delta_g

        # RS 値の更新
        self.rs = t * (self.Q.T - self.r).T
         
        return   q_current_state, q_next_state, next_action , eligibility    

    def GRC_update(self, e_tmp):
        self.e_g = (e_tmp + self.gamma_g * (self.n_g * self.e_g)) / float(1 + self.gamma_g * self.n_g)
        # self.e_g = e_tmp
        self.n_g = 1.0 + self.gamma_g * self.n_g
        self.sum_Eg.append(self.e_g)

    def init_params(self):
        super().init_params()

        self.t_curr = np.zeros([self.num_state, self.num_action])
        self.t_post = np.zeros([self.num_state, self.num_action])
        self.rs = np.zeros([self.num_state, self.num_action])
        self.r = np.zeros(self.num_state)
        self.e_g = 0

    def get_value(self):
        return self.rs

"""# RS + GRC + 適格度トレース(on policy)"""

class RS_lam_on(Sarsa_lambda):
    def __init__(self, num_state, num_action, learning_rate=0.1, discount_rate=0.9, r_g=0.75, zeta=0.05):

        super().__init__(num_state, num_action, learning_rate, discount_rate)

        self.r = []
        self.t_curr = []
        self.t_post = []
        self.rs = []
        self.zeta = zeta
        self.r_g = r_g
        self.gamma_g = 0.9
        self.e_g = 0
        self.n_g = 0
        self.sum_Eg = []
        self.tau = 0

    def update(self, current_state, current_action, reward, next_state, next_action):
        alpha_tau = self.learning_rate
        gamma_tau = self.discount_rate
        # alpha_tau = 0.1
        # gamma_tau = 0.4

        # Q 値の更新を行う
        q_current_state, q_next_state, next_action , eligibility= super().update(current_state, current_action, reward, next_state, next_action)

        # tau 値の更新
        t = self.t_curr + self.t_post
        self.t_curr[q_current_state, current_action] += 1
        self.t_post[q_current_state, current_action] += alpha_tau * (gamma_tau * t[q_next_state, next_action] - self.t_post[q_current_state, current_action])
        t = self.t_curr + self.t_post
        self.tau = t
        # t = self.t_curr + gamma_tau * 0.9 * eligibility

        # 基準値 R の更新
        delta_g = min(self.e_g - self.r_g, 0)
        a = self.estimate_policy.select_action(self.Q, q_current_state)
        self.r[q_current_state] = self.Q[q_current_state][a] - self.zeta * delta_g

        # RS 値の更新
        self.rs = t * (self.Q.T - self.r).T

    def GRC_update(self, e_tmp):
        self.e_g = (e_tmp + self.gamma_g * (self.n_g * self.e_g)) / float(1 + self.gamma_g * self.n_g)
        # self.e_g = e_tmp
        self.n_g = 1.0 + self.gamma_g * self.n_g
        self.sum_Eg.append(self.e_g)

    def init_params(self):
        super().init_params()

        self.t_curr = np.zeros([self.num_state, self.num_action])
        self.t_post = np.zeros([self.num_state, self.num_action])
        self.rs = np.zeros([self.num_state, self.num_action])
        self.r = np.zeros(self.num_state)
        self.e_g = 0
        
    def get_value(self):
        return self.rs

class RS_tau_off(Watkin_lambda):
    def __init__(self, num_state, num_action, learning_rate=0.1, discount_rate=0.9, r_g=0.75, zeta=0.05):

        super().__init__(num_state, num_action, learning_rate, discount_rate)

        self.r = []
        self.tau = np.zeros([self.num_state, self.num_action])
        self.rs = []
        self.zeta = zeta
        self.r_g = r_g
        self.gamma_g = 0.9
        self.e_g = 0
        self.n_g = 0
        self.sum_Eg = []

    def update(self, current_state, current_action, reward, next_state, next_action):
        alpha_tau = self.learning_rate
        gamma_tau = self.discount_rate

        # Q 値の更新を行う
        q_current_state, q_next_state, next_action , eligibility = super().update(current_state, current_action, reward, next_state, next_action)

        # tau 値の更新
        if reward != 0:
            print("not goal")
            delta = 0 + gamma_tau * self.tau[q_next_state, next_action] - self.tau[q_current_state, current_action]
        else:
            print("goal")
            delta = 1 + gamma_tau * self.tau[q_next_state, next_action] - self.tau[q_current_state, current_action]
        self.tau += alpha_tau * delta 

        # 基準値 R の更新
        delta_g = min(self.e_g - self.r_g, 0)
        a = self.estimate_policy.select_action(self.Q, q_current_state)
        self.r[q_current_state] = self.Q[q_current_state][a] - self.zeta * delta_g

        # RS 値の更新
        self.rs = self.tau * (self.Q.T - self.r).T
         
        return   q_current_state, q_next_state, next_action , eligibility    

    def GRC_update(self, e_tmp):
        self.e_g = (e_tmp + self.gamma_g * (self.n_g * self.e_g)) / float(1 + self.gamma_g * self.n_g)
        # self.e_g = e_tmp
        self.n_g = 1.0 + self.gamma_g * self.n_g
        self.sum_Eg.append(self.e_g)

    def init_params(self):
        super().init_params()

        self.tau = np.zeros([self.num_state, self.num_action])
        self.rs = np.zeros([self.num_state, self.num_action])
        self.r = np.zeros(self.num_state)
        self.e_g = 0

    def get_value(self):
        return self.rs

    
class RS_tau_on(Sarsa_lambda):
    def __init__(self, num_state, num_action, learning_rate=0.1, discount_rate=0.9, r_g=0.75, zeta=0.05):

        super().__init__(num_state, num_action, learning_rate, discount_rate)

        self.r = []
        self.t_curr = []
        self.t_post = []
        self.rs = []
        self.zeta = zeta
        self.r_g = r_g
        self.gamma_g = 0.9
        self.e_g = 0
        self.n_g = 0
        self.sum_Eg = []
        self.tau = 0

    def update(self, current_state, current_action, reward, next_state, next_action):
        alpha_tau = self.learning_rate
        gamma_tau = self.discount_rate

        # Q 値の更新を行う
        q_current_state, q_next_state, next_action , eligibility = super().update(current_state, current_action, reward, next_state, next_action)

        # tau 値の更新
        t = self.t_curr + self.t_post
        self.t_curr[q_current_state, current_action] += 1
        self.t_post[q_current_state, current_action] += alpha_tau * (gamma_tau * t[q_next_state, next_action] - self.t_post[q_current_state, current_action])
        # t = self.t_curr + self.t_post
        t = self.t_curr + gamma_tau * eligibility
        self.tau = t

        # 基準値 R の更新
        delta_g = min(self.e_g - self.r_g, 0)
        a = self.estimate_policy.select_action(self.Q, q_current_state)
        self.r[q_current_state] = self.Q[q_current_state][a] - self.zeta * delta_g

        # RS 値の更新
        self.rs = t * (self.Q.T - self.r).T
         
        return   q_current_state, q_next_state, next_action , eligibility    

    def GRC_update(self, e_tmp):
        self.e_g = (e_tmp + self.gamma_g * (self.n_g * self.e_g)) / float(1 + self.gamma_g * self.n_g)
        # self.e_g = e_tmp
        self.n_g = 1.0 + self.gamma_g * self.n_g
        self.sum_Eg.append(self.e_g)

    def init_params(self):
        super().init_params()

        self.t_curr = np.zeros([self.num_state, self.num_action])
        self.t_post = np.zeros([self.num_state, self.num_action])
        self.rs = np.zeros([self.num_state, self.num_action])
        self.r = np.zeros(self.num_state)
        self.e_g = 0

    def get_value(self):
        return self.rs