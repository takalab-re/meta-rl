import numpy as np
import matplotlib.pyplot as plt

r_list_total = []
r_list_d_total = []

sim = 2
for i in range(1, sim):
    #行は状態、列は移動方向で↑、→、↓、←を表す
    theta_0 = np.array([[np.nan, 1, 1, np.nan], #S0
                         [np.nan, 1, 1, 1], #S1
                         [np.nan, 1, 1, 1], #S2　
                         [np.nan, np.nan, 1, 1], #S3
                         [1, 1, 1, np.nan], #S4
                         [1, 1, 1, 1], #S5
                         [1, 1, 1, 1], #S6
                         [1, np.nan, 1, 1], #S7
                         [1, 1, 1, np.nan], #S8
                         [1, 1, 1, 1], #S9
                         [1, 1, 1, 1], #S10
                         [1, np.nan, 1, 1], #S11
                         [1, 1, np.nan, np.nan], #S12
                         [1, 1, np.nan, 1], #S13
                         [1, 1, np.nan, 1], #S14, S15はゴールのため方策なし
                         ])

    [a, b] = theta_0.shape #行と列の数をa, bに格納
    Q = np.random.rand(a, b) * theta_0 * 0.1
    Q_d = np.random.rand(a, b) * theta_0 * 0.1
    #*theta_0をすることで要素ごとに掛け算し、Qの壁方向の値がnanになる。

    def simple_convert_into_pi_from_theta(theta):
        #単純に割合を計算
        [m, n] = theta.shape #thetaの行列サイズを取得
        pi = np.zeros((m, n))
        for i in range(0, m):
            pi[i, :] = theta[i, :] / np.nansum(theta[i, :]) #割合の計算

        pi = np.nan_to_num(pi) #nanを0に変換

        return pi

    pi_0 = simple_convert_into_pi_from_theta(theta_0)
    pi_0_d = simple_convert_into_pi_from_theta(theta_0)

    def get_action(s, Q, epsilon, pi_0):
        direction = ["up", "right", "down", "left"]

        #egreedy
        if np.random.rand() < epsilon:
            next_direction = np.random.choice(direction, p=pi_0[s, :]) #eの確率でランダム行動
        else:
            next_direction = direction[np.nanargmax(Q[s, :])] #Qの最大値の行動

        if next_direction == "up":
            action = 0
        elif next_direction == "right":
            action = 1
        elif next_direction == "down":
            action = 2
        elif next_direction == "left":
            action = 3

        return action

    #1step移動後の状態sを求める
    def get_s_next(s, a, Q, epsilon, pi_0):
        direction = ["up", "right", "down", "left"]

        next_direction = direction[a] #行動aの方向

        if next_direction == "up":
          s_next = s - 4
        elif next_direction == "right":
          s_next = s + 1
        elif next_direction == "down":
          s_next = s + 4
        elif next_direction == "left":
          s_next = s - 1

        return s_next

    def Q_learning(s, a, r, s_next, Q, eta, gamma):
        if s_next == 15:
            Q[s, a] = Q[s, a] + eta * (r - Q[s, a])
        else:
            Q[s, a] = Q[s, a] + eta * (r + gamma * np.nanmax(Q[s_next, :]) - Q[s, a])

        return Q

    #迷路内をエージェントがゴールするまで移動させる関数の定義
    def goal_maze_ret_s_a_Q(Q, epsilon, eta, gamma, pi):
        s = 0 #スタート地点
        a = a_next = get_action(s, Q, epsilon, pi) #初期の行動
        s_a_history = [[0, np.nan]] #エージェントの移動を記録するリスト
        #r = 0

        while(1):
            a = a_next #行動更新

            s_a_history[-1][1] = a #現在の状態に行動を代入

            s_next = get_s_next(s, a, Q, epsilon, pi) #次の状態を格納

            s_a_history.append([s_next, np.nan]) #次の状態を代入。行動はまだわからないのでnanにしておく

            if s_next == 15:
                r = 1
                #r = 1 / (len(s_a_history) - 1)
                a_next = np.nan
                break
            else:
                r = 0
                a_next = get_action(s_next, Q, epsilon, pi) #次の行動a_nextを求める

            Q = Q_learning(s, a, r, s_next, Q, eta, gamma) #価値関数を更新

            if s_next == 15:
                break
            else:
                s = s_next

        return [s_a_history, Q, r]

    #迷路内をエージェントがゴールするまで移動させる関数の定義
    def goal_maze_ret_s_a_Q_d(Q, epsilon, eta, gamma, pi):
        s = 0 #スタート地点
        a = a_next = get_action(s, Q, epsilon, pi) #初期の行動
        s_a_history = [[0, np.nan]] #エージェントの移動を記録するリスト
        #r = 0

        while(1):
            a = a_next #行動更新

            s_a_history[-1][1] = a #現在の状態に行動を代入

            s_next = get_s_next(s, a, Q, epsilon, pi) #次の状態を格納

            s_a_history.append([s_next, np.nan]) #次の状態を代入。行動はまだわからないのでnanにしておく

            if s_next == 15:
                #r = 1
                r = 1 / (len(s_a_history) - 1)
                a_next = np.nan
                break
            else:
                r = 0
                a_next = get_action(s_next, Q, epsilon, pi) #次の行動a_nextを求める

            Q = Q_learning(s, a, r, s_next, Q, eta, gamma) #価値関数を更新

            if s_next == 15:
                break
            else:
                s = s_next

        return [s_a_history, Q, r]


    print("sim : ", str(i))
    arefg = 1 / 6 #大局的満足基準度
    Eg = 0 #δgステップ平均報酬
    eg = 0 #εステップ平均報酬
    eta = 0.1 #学習率
    gamma = 0.99 #割引率
    eps = 1
    dg_eps = 1
    dg = 1
    episode = 200

    j_list = [] #プロット用
    r_list = [] #プロット用
    r_list_d = [] #プロット用

    step_list = []
    step_list_d = []

    dg_list = []

    for j in range(1, episode):
        j_list.append(j) #episodeのリスト

        #dg_eps = max(1 - Eg/arefg, 0)
        dg_eps = abs(min(dg, 0) / arefg)
        #print('dg_eps : ', dg_eps)

        [s_a_history, Q, r] = goal_maze_ret_s_a_Q(Q, eps, eta, gamma, pi_0) #迷路を解き、移動した履歴と更新したQを求める
        [s_a_history_d, Q_d, Eg] = goal_maze_ret_s_a_Q_d(Q_d, dg_eps, eta, gamma, pi_0_d) #迷路を解き、移動した履歴と更新したQを求める

        #eg = r / (len(s_a_history)-1) #εのステップ平均報酬
        #Eg = r_d / (len(s_a_history_d)-1) #δgのステップ平均報酬

        dg_list.append(dg)

        dg = Eg - arefg
        #dg = r_d - arefg

        #rr += eg
        r_list.append(r/(len(s_a_history)-1))
        #r_list.append(len(s_a_history)-1)
        #rr_d += Eg
        r_list_d.append(Eg)
        #r_list_d.append(len(s_a_history_d)-1)
        print(j, str(len(s_a_history_d)-1))

        if eps > 0:
            eps = eps - 0.01

    r_list_total.append(r_list)
    r_list_d_total.append(r_list_d)

    if 0 < i < 11:
        plt.title(i)
        plt.plot(j_list, dg_list, label='δg')
        plt.xlabel("episode")
        plt.ylabel("ε, δg transition")
        plt.grid()
        plt.legend()
        plt.show()

plt.plot(j_list, np.mean(r_list_total, axis=0), label='εGreedy')
plt.plot(j_list, np.mean(r_list_d_total, axis=0), label='δgGreedy')
plt.xlabel("episode")
plt.ylabel("Reward per episode")
plt.grid()
plt.legend()
plt.show()
