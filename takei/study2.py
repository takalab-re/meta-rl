import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import random
import pandas as pd
import seaborn as sns
import csv


def simple_convert_into_pi_from_theta(theta): #単純に割合を求める

    [m, n] = theta.shape  # thetaの行列サイズを取得
    pi = np.zeros((m, n))
    for i in range(0, m):
        pi[i, :] = theta[i, :] / np.nansum(theta[i, :])  # 割合の計算

    pi = np.nan_to_num(pi)  # nanを0に変換

    return pi

def get_action(s, Q, epsilon, pi_0):
    direction = ["up", "right", "down", "left"]

    # 行動を決める
    if np.random.rand() < epsilon:
        # εの確率でランダムに動く
        next_direction = np.random.choice(direction, p=pi_0[s, :])
    else:
        # Qの最大値の行動を採用する
        next_direction = direction[np.nanargmax(Q[s, :])]

    # 行動をindexに
    if next_direction == "up":
        action = 0
    elif next_direction == "right":
        action = 1
    elif next_direction == "down":
        action = 2
    elif next_direction == "left":
        action = 3

    return action


def get_s_next(s, a, Q, epsilon, pi_0, env):
    direction = ["up", "right", "down", "left"]
    next_direction = direction[a]  # 行動aの方向

    # 行動から次の状態を決める
    if next_direction == "up":
        s_next = s - env  # 上に移動するときは状態の数字が3小さくなる
    elif next_direction == "right":
        s_next = s + 1  # 右に移動するときは状態の数字が1大きくなる
    elif next_direction == "down":
        s_next = s + env  # 下に移動するときは状態の数字が3大きくなる
    elif next_direction == "left":
        s_next = s - 1  # 左に移動するときは状態の数字が1小さくなる

    return s_next

def Q_learning(s, a, r, s_next, Q, eta, gamma, env):

    if s_next == env*env-1:  # ゴールした場合
        Q[s, a] = Q[s, a] + eta * (r - Q[s, a])
    else:  #更新式
        Q[s, a] = Q[s, a] + eta * (r + gamma * np.nanmax(Q[s_next,: ]) - Q[s, a])

    return Q

def goal_maze_ret_s_a_Q(Q, epsilon, eta, gamma, pi, env):
    s = 0  # スタート地点
    a = a_next = get_action(s, Q, epsilon, pi)  # 初期の行動
    s_a_history = [[0, np.nan]]  # エージェントの移動を記録するリスト

    while (1):  # ゴールするまでループ
        a = a_next  # 行動更新

        s_a_history[-1][1] = a
        # 現在の状態（つまり一番最後なのでindex=-1）に行動を代入

        s_next = get_s_next(s, a, Q, epsilon, pi, env)
        # 次の状態を格納

        s_a_history.append([s_next, np.nan])
        # 次の状態を代入。行動はまだ分からないのでnanにしておく

        # 報酬を与え,　次の行動を求めます
        if s_next == env*env-1:
            r = 1  # ゴールにたどり着いたなら報酬を与える
            a_next = np.nan
        else:
            r = 0
            a_next = get_action(s_next, Q, epsilon, pi)
            # 次の行動a_nextを求めます。

        # 価値関数を更新
        Q = Q_learning(s, a, r, s_next, Q, eta, gamma, env)

        # 終了判定
        if s_next == env*env-1:  # ゴール地点なら終了
            break
        else:
            s = s_next


    return s_a_history, Q, r

def goal_maze_ret_s_a_Q_d(Q, epsilon, eta, gamma, pi, env):
    s = 0 #スタート地点
    w = 18 #加重10*10
    a = a_next = get_action(s, Q, epsilon, pi) #初期の行動
    s_a_history = [[0, np.nan]] #エージェントの移動を記録するリスト
    #r = 0

    while(1):
        a = a_next #行動更新

        s_a_history[-1][1] = a #現在の状態に行動を代入

        s_next = get_s_next(s, a, Q, epsilon, pi, env) #次の状態を格納

        s_a_history.append([s_next, np.nan]) #次の状態を代入。行動はまだわからないのでnanにしておく

        if s_next == env*env-1:
            #r = 1
            r = 1 / (len(s_a_history) - 1)
            a_next = np.nan
            # w0 = (len(s_a_history)-1) - w
            # if w0 == 0:
            #     r = 1 / (len(s_a_history) - 1)
            #     a_next = np.nan
            #     #break
            # else:
            #     w0 = w0 / 100
            #     r = (1 - w0) / (len(s_a_history) - 1)
            #     a_next = np.nan
        else:
            r = 0
            a_next = get_action(s_next, Q, epsilon, pi) #次の行動a_nextを求める

        Q = Q_learning(s, a, r, s_next, Q, eta, gamma, env) #価値関数を更新

        if s_next == env*env-1:
            break
        else:
            s = s_next

    return s_a_history, Q, r

def main(env):
    r_list_total = []
    r_list_d_total = []
    dg_list_total = []
    dg_eps_list_total = []
    sim = 2

    for i in range(1, sim):
        # 行動方策pi_0を求める
        pi_0 = simple_convert_into_pi_from_theta(theta_0)
        pi_0_d = simple_convert_into_pi_from_theta(theta_0)

        [a, b] = theta_0.shape  # 行と列の数をa, bに格納
        Q = np.random.rand(a, b) * theta_0 * 0.1
        Q_d = np.random.rand(a, b) * theta_0 * 0.1
        #*theta_0をすることで要素ごとに掛け算し、Qの壁方向の値がnanになる。

        #arefg = 1 / 6 #大局的満足基準度 4*4
        arefg = 1 / 18 #大局的満足基準度 10*10
        #arefg = 1 / 28 #大局的満足基準度 15*15
        Eg = 0 #δgステップ平均報酬
        eg = 0 #εステップ平均報酬

        eta = 0.1  # 学習率
        gamma = 0.9  # 時間割引率

        eps = 1.0  #ε-greedyの初期値
        dg_eps = 1.0

        dg = Eg - arefg

        episode = 2001

        r_list = []
        r_list_d = [] #プロット用

        dg_list = []
        dg_eps_list = []

        print("sim : ", i)
        for j in range(1, episode):
            #dg_eps = max(1 - Eg/arefg, 0)
            dg_eps = abs(min(dg, 0) / arefg)

            # Q学習で迷路を解き、移動した履歴と更新したQを求める
            s_a_history, Q, r = goal_maze_ret_s_a_Q(Q, eps, eta, gamma, pi_0, env)
            s_a_history_d, Q_d, Eg = goal_maze_ret_s_a_Q_d(Q_d, dg_eps, eta, gamma, pi_0_d, env) #迷路を解き、移動した履歴と更新したQを求める

            dg_list.append(dg)
            dg_eps_list.append(dg_eps)

            dg = Eg - arefg

            r_list.append(r/(len(s_a_history)-1))
            #r_list.append(r)
            r_list_d.append(Eg)

            # 100エピソード繰り返す
            if eps > 0:
                if eps < 0.005:
                    eps = 0.0
                else:
                    eps = eps - 0.005
            #print(len(s_a_history_d)-1)

        r_list_total.append(r_list)
        r_list_d_total.append(r_list_d)
        dg_list_total.append(dg_list)
        dg_eps_list_total.append(dg_eps_list)

        if 0 < i < 11:
            plt.title(i)
            plt.plot(range(1, episode), dg_list, label='δg')
            plt.xlabel("episode")
            plt.ylabel("δg (Before conversion)")
            plt.grid()
            plt.legend()
            plt.show()

            plt.title(i)
            plt.plot(range(1, episode), dg_eps_list, label='δg_eps')
            plt.xlabel("episode")
            plt.ylabel("δg (After conversion)")
            plt.grid()
            plt.legend()
            plt.show()

    plt.title("Average of " +str(sim-1)+ " simulations")
    plt.plot(range(1, episode), np.mean(r_list_total, axis=0), label='εGreedy')
    plt.plot(range(1, episode), np.mean(r_list_d_total, axis=0), label='δgGreedy')
    plt.xlabel("episode")
    plt.ylabel("Reward per step")
    plt.grid()
    plt.legend()
    plt.show()

    plt.title("Average of " +str(sim-1)+ " simulations")
    plt.plot(range(1, episode), np.mean(dg_list_total, axis=0), label='δg Average')
    plt.xlabel("episode")
    plt.ylabel("δg (Before conversion)")
    plt.grid()
    plt.legend()
    plt.show()

    plt.title("Average of " +str(sim-1)+ " simulations")
    plt.plot(range(1, episode), np.mean(dg_eps_list_total, axis=0), label='δg_eps Average')
    plt.xlabel("episode")
    plt.ylabel("δg (After conversion)")
    plt.grid()
    plt.legend()
    plt.show()

    # plt.figure()
    # sns.heatmap(Q)
    # plt.savefig('a.png')
    # plt.close('all')

    with open('data.csv', 'w') as file:
        writer = csv.writer(file, lineterminator = '\n')
        writer.writerows(Q)

    with open('data2.csv', 'w') as file:
        writer = csv.writer(file, lineterminator = '\n')
        writer.writerows(Q_d)

    #print(dg_list_total)
    # print("Q : ", Q)
    # print("Q_d : ", Q_d)

if __name__ == '__main__':
    #行は状態、列は移動方向で↑、→、↓、←を表す
    # theta_0 = np.array([[np.nan, 1, 1, np.nan], #S0
    #                      [np.nan, 1, 1, 1], #S1
    #                      [np.nan, 1, 1, 1], #S2　
    #                      [np.nan, np.nan, 1, 1], #S3
    #                      [1, 1, 1, np.nan], #S4
    #                      [1, 1, 1, 1], #S5
    #                      [1, 1, 1, 1], #S6
    #                      [1, np.nan, 1, 1], #S7
    #                      [1, 1, 1, np.nan], #S8
    #                      [1, 1, 1, 1], #S9
    #                      [1, 1, 1, 1], #S10
    #                      [1, np.nan, 1, 1], #S11
    #                      [1, 1, np.nan, np.nan], #S12
    #                      [1, 1, np.nan, 1], #S13
    #                      [1, 1, np.nan, 1], #S14, S15はゴールのため方策なし
    #                      ])
    # env = 4

    theta_0 = np.array([[np.nan, 1, 1, np.nan], #S0
                         [np.nan, 1, 1, 1], #S1
                         [np.nan, 1, 1, 1], #S2　
                         [np.nan, 1, 1, 1], #S3
                         [np.nan, 1, 1, 1], #S4
                         [np.nan, 1, 1, 1], #S5
                         [np.nan, 1, 1, 1], #S6
                         [np.nan, 1, 1, 1], #S7
                         [np.nan, 1, 1, 1], #S8
                         [np.nan, np.nan, 1, 1], #S9
                         [1, 1, 1, np.nan], #S10
                         [1, 1, 1, 1], #S11
                         [1, 1, 1, 1], #S12
                         [1, 1, 1, 1], #S13
                         [1, 1, 1, 1], #S14
                         [1, 1, 1, 1], #S15
                         [1, 1, 1, 1], #S16
                         [1, 1, 1, 1], #S17
                         [1, 1, 1, 1], #S18
                         [1, np.nan, 1, 1], #S19
                         [1, 1, 1, np.nan], #S20
                         [1, 1, 1, 1], #S21
                         [1, 1, 1, 1], #S22
                         [1, 1, 1, 1], #S23
                         [1, 1, 1, 1], #S24
                         [1, 1, 1, 1], #S25
                         [1, 1, 1, 1], #S26
                         [1, 1, 1, 1], #S27
                         [1, 1, 1, 1], #S28
                         [1, np.nan, 1, 1], #S29
                         [1, 1, 1, np.nan], #S30
                         [1, 1, 1, 1], #S31
                         [1, 1, 1, 1], #S32
                         [1, 1, 1, 1], #S33
                         [1, 1, 1, 1], #S34
                         [1, 1, 1, 1], #S35
                         [1, 1, 1, 1], #S36
                         [1, 1, 1, 1], #S37
                         [1, 1, 1, 1], #S38
                         [1, np.nan, 1, 1], #S39
                         [1, 1, 1, np.nan], #S40
                         [1, 1, 1, 1], #S41
                         [1, 1, 1, 1], #S42
                         [1, 1, 1, 1], #S43
                         [1, 1, 1, 1], #S44
                         [1, 1, 1, 1], #S45
                         [1, 1, 1, 1], #S46
                         [1, 1, 1, 1], #S47
                         [1, 1, 1, 1], #S48
                         [1, np.nan, 1, 1], #S49
                         [1, 1, 1, np.nan], #S50
                         [1, 1, 1, 1], #S51
                         [1, 1, 1, 1], #S52
                         [1, 1, 1, 1], #S53
                         [1, 1, 1, 1], #S54
                         [1, 1, 1, 1], #S55
                         [1, 1, 1, 1], #S56
                         [1, 1, 1, 1], #S57
                         [1, 1, 1, 1], #S58
                         [1, np.nan, 1, 1], #S59
                         [1, 1, 1, np.nan], #S60
                         [1, 1, 1, 1], #S61
                         [1, 1, 1, 1], #S62
                         [1, 1, 1, 1], #S63
                         [1, 1, 1, 1], #S64
                         [1, 1, 1, 1], #S65
                         [1, 1, 1, 1], #S66
                         [1, 1, 1, 1], #S67
                         [1, 1, 1, 1], #S68
                         [1, np.nan, 1, 1], #S69
                         [1, 1, 1, np.nan], #S70
                         [1, 1, 1, 1], #S71
                         [1, 1, 1, 1], #S72
                         [1, 1, 1, 1], #S73
                         [1, 1, 1, 1], #S74
                         [1, 1, 1, 1], #S75
                         [1, 1, 1, 1], #S76
                         [1, 1, 1, 1], #S77
                         [1, 1, 1, 1], #S78
                         [1, np.nan, 1, 1], #S79
                         [1, 1, 1, np.nan], #S80
                         [1, 1, 1, 1], #S81
                         [1, 1, 1, 1], #S82
                         [1, 1, 1, 1], #S83
                         [1, 1, 1, 1], #S84
                         [1, 1, 1, 1], #S85
                         [1, 1, 1, 1], #S86
                         [1, 1, 1, 1], #S87
                         [1, 1, 1, 1], #S88
                         [1, np.nan, 1, 1], #S89
                         [1, 1, np.nan, np.nan], #S90
                         [1, 1, np.nan, 1], #S91
                         [1, 1, np.nan, 1], #S92
                         [1, 1, np.nan, 1], #S93
                         [1, 1, np.nan, 1], #S94
                         [1, 1, np.nan, 1], #S95
                         [1, 1, np.nan, 1], #S96
                         [1, 1, np.nan, 1], #S97
                         [1, 1, np.nan, 1], #S98, S99はゴールのため方策なし
                         ])
    env = 10
    
    main(env)